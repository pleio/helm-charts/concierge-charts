# concierge

![Version: 0.1.2](https://img.shields.io/badge/Version-0.1.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

Helm chart for deploying concierge

## Source Code

* <https://gitlab.com/pleio/helm-charts/concierge-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| analytics.matomo.site_id | string | `nil` |  |
| analytics.matomo.url | string | `nil` |  |
| background.scheduler.resources | object | `{}` |  |
| background.worker.replicaCount | int | `1` |  |
| background.worker.resources | object | `{}` |  |
| common.timeZone | string | `"CET"` |  |
| config.celery.brokerUrl | string | `nil` |  |
| config.database.host | string | `nil` |  |
| config.database.name | string | `nil` |  |
| config.database.password | string | `nil` |  |
| config.database.user | string | `nil` |  |
| config.email.host | string | `nil` |  |
| config.email.password | string | `nil` |  |
| config.email.port | string | `nil` |  |
| config.email.user | string | `nil` |  |
| config.geoip.licenseKey | string | `nil` |  |
| config.recaptcha.secretKey | string | `nil` |  |
| config.recaptcha.siteKey | string | `nil` |  |
| config.secretKey | string | `nil` |  |
| domain | string | `"account2.pleio-test.nl"` |  |
| existingSecret | string | `nil` |  |
| image.pullPolicy | string | `"Always"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/concierge"` |  |
| image.tag | string | `"latest"` |  |
| ingress.annotations."kubernetes.io/ingress.class" | string | `"nginx"` |  |
| ingress.annotations."nginx.ingress.kubernetes.io/proxy-body-size" | string | `"250m"` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| service.port | int | `8000` |  |
| storage.className | string | `"efs"` |  |
| storage.size | string | `"50Gi"` |  |
| useLetsEncryptCertificate | bool | `false` |  |
| wildcardTlsSecretName | string | `""` |  |
